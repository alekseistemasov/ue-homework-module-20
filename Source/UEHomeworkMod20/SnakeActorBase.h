// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActorBase.generated.h"


class ASnakeElementBase;

UENUM()
enum class EMovementDirection : uint8 {
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class UEHOMEWORKMOD20_API ASnakeActorBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActorBase();

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElementsArr;

	UPROPERTY(EditDefaultsOnly)
	float ElementSize;

	UPROPERTY()
	EMovementDirection LastDirection;

	UPROPERTY(EditDefaultsOnly)
	float Speed;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddSnakeElements(int elementsNum);

	void Move();

};
