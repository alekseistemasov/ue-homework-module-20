// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Camera/CameraComponent.h"
#include "SnakeActorBase.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	RootComponent = Camera;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	CreateSnakeActor();
	

	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandleVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandleHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeActorBase>(SnakeActorClass, FTransform(FVector(0, 0, 60)));
}

void APlayerPawnBase::HandleVerticalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastDirection != EMovementDirection::DOWN) {
			SnakeActor->LastDirection = EMovementDirection::UP;
		}
		else if (value < 0 && SnakeActor->LastDirection != EMovementDirection::UP) {
			SnakeActor->LastDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HandleHorizontalInput(float value)
{
	if (IsValid(SnakeActor)) {
		if (value > 0 && SnakeActor->LastDirection != EMovementDirection::LEFT) {
			SnakeActor->LastDirection = EMovementDirection::RIGHT;
		}
		else if (value < 0 && SnakeActor->LastDirection != EMovementDirection::RIGHT) {
			SnakeActor->LastDirection = EMovementDirection::LEFT;
		}
	}
}

