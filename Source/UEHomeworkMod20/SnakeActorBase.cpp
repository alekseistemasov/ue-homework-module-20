// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeActorBase.h"
#include "SnakeElementBase.h"

// Sets default values
ASnakeActorBase::ASnakeActorBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 150;
	Speed = 1;
	LastDirection = EMovementDirection::DOWN;

}

// Called when the game starts or when spawned
void ASnakeActorBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElements(4);
	SetActorTickInterval(Speed);
	
}

// Called every frame
void ASnakeActorBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();

}

void ASnakeActorBase::AddSnakeElements(int elementsNum)
{
	for (int i = 0; i < elementsNum; i++) {
		FVector newLocation(SnakeElementsArr.Num() * ElementSize, 0, 60);
		FTransform newTransform(newLocation);
		ASnakeElementBase* element = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, newTransform);
		SnakeElementsArr.Add(element);
		if (i == 0) {
			element->SetFirstElementType();
		}
	}
}

void ASnakeActorBase::Move()
{
	FVector direction(ForceInitToZero);

	
	switch (LastDirection) {
	case EMovementDirection::UP:
		direction.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		direction.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		direction.Y -= ElementSize;
		break;
	case EMovementDirection::RIGHT:
		direction.Y += ElementSize;
		break;
	default:
		direction.X -= ElementSize;
		break;
		
	}

	for (int i = SnakeElementsArr.Num() - 1; i > 0; i--) {
		auto currentElement = SnakeElementsArr[i];
		auto prevElement = SnakeElementsArr[i - 1];
		currentElement->SetActorLocation(prevElement->GetActorLocation());

	}

	SnakeElementsArr[0]->AddActorWorldOffset(direction);
}

